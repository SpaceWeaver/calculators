package Calculators;
import java.util.Scanner;

/**
 * Basic calculator used to process speed, distance and time calculations. Uses Kilometers, Hours and KM/H as the results
 * @author Tyler Baxandall, tyler.baxandall@hotmail.com
 *
 */
public class SpeedDistTimeCalc {

	/*
	 * Main menu for the Speed, Distance and Time Calculator
	 */
	void Menu(){
		
		//value preset to start the menu loop
		int input = 1;

		while(input != 0) {


			System.out.println("Welcome to the Speed, Distance and Time Calculator!");
			System.out.println("Would you like the average speed based on distance and time(1)");
			System.out.println("Time to cover a distance at a specific speed(2)");
			System.out.println("Calculate the distance based on speed and time (3)");
			System.out.println("Or Exit (0)");

			input = Utilities.GetIntInput("\nPlease enter one of the integers specified in the menu",
					"Non Integer value entered, please reenter");

			//confirm that the input is one of the options
			while(input < 0 || input > 3){
				System.out.println("Wrong input, please select a value between 0 and 3");
				input = Utilities.GetIntInput("Please enter one of the integers specified in the menu",
						"Non Integer value entered, please reenter");;
			}

			switch (input) {
			case 1: CalculateKmH();
			break;
			case 2: CalculateTime();
			break;
			case 3: CalculateDistance();
			break;
			case 0:
			System.out.println("Thank you for using the Speed,Distance and Time Calculator!!");
			}

		}
	}
	
	/**
	 * Prompts user for Distance in KM and time in Hours, then calculates the speed in KM/h
	 */
	void CalculateKmH() {
		double distance;
		double hours;
		
		distance = Utilities.GetDoubleInput("\nPlease enter the distance in Kilometers", 
				"Input was not a numerical value, please reenter");
		
		hours = Utilities.GetDoubleInput("Please enter the time in Hours",
				"Input was not a numerical value, please reenter");
		
		double speed = distance / hours;
		
		//Printout Speed in kmh, to 2 decimal places
		System.out.println("To cover " + distance + " km in " + hours + " hours, "
				+ "the vehicle will need to move at ");
		System.out.printf("%.2f", speed );
		System.out.print(" km/h\n\n");
	
	}
	
	/**
	 * Takes input for distance and speed in KM and KM/H respectively and calculates the time. 
	 */
	void CalculateTime() {
		double distance;
		double speed;
		
		distance = Utilities.GetDoubleInput("\nPlease enter the distance in Kilometers", 
				"Input was not a numerical value please reenter");
		
		speed = Utilities.GetDoubleInput("Please enter the speed in Km/h",
				"Input was not a numerical value, please reenter");
		
		double time = distance / speed;
		
		
		//Printout Time in Hours, to 2 decimal places
		System.out.println("It will take ");
		System.out.printf("%.2f", time);
		System.out.print(" hours to cover " + distance + " KM at " + speed +" KM/H\n\n" );
		
	}
	
	/**
	 * Prompts user for Time in Hours and Speed in Km/H, Calculates distance based upon these.
	 */
	void CalculateDistance() {
		double speed;
		double time;
		
		time = Utilities.GetDoubleInput("\nPlease enter the Time in Hours",
				"Input was not a numerical value, please reenter");
		
		speed = Utilities.GetDoubleInput("Please enter the speed in km/h",
				"Input was not a numerical value, please reenter");
		
		double distance = speed * time;
		
		//Printout Distance in km, to 2 decimal places
		System.out.println("Moving at " + speed + " km/h, for " + time +
				" hours will allow you to cover a distance of ");
		System.out.printf("%.2f",distance);
		System.out.print(" km\n\n");
	}
	
	
}
