package Calculators;

import java.util.Scanner;

/**
 * A Class to contain utility methods such as various types of input
 * @author Tyler Baxandall / tyler.baxandall@hotmail.com
 *
 */
public class Utilities {

	/**
	 * Input validator, uses an input message to prompt user and error message for invalid input types,
	 * once the user inputs a double it is returned to the caller
	 * @param InputMessage
	 * @param ErrorMessage
	 * @return
	 */
	static double GetDoubleInput(String InputMessage, String ErrorMessage) {
		System.out.println(InputMessage);
		Scanner Input = new Scanner(System.in);
		while(!Input.hasNextDouble()){
			System.out.print(ErrorMessage);
			Input.next();
		}
		
		return Input.nextDouble();
	}
	
	/**
	 * Input validator, uses an input message to prompt user and error message for invalid input types,
	 * once the user inputs a Integer it is returned to the caller
	 * @param InputMessage
	 * @param ErrorMessage
	 * @return
	 */
	static int GetIntInput(String InputMessage, String ErrorMessage) {
		System.out.println(InputMessage);
		Scanner Input = new Scanner(System.in);
		while(!Input.hasNextInt()){
			System.out.print(ErrorMessage);
			Input.next();
		}
		
		return Input.nextInt();
	}
}
